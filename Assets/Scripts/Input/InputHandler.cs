using Shooter.Game;
using Shooter.Services;
using Shooter.Services.Events;
using UniRx;
using UnityEngine;


namespace Shooter.Input
{
    public class InputHandler : MonoBehaviour
    {
        #region Fields

        private IEventsService eventsService;

        #endregion



        #region Unity lifecycle

        private void Awake()
        {
            eventsService = ServicesHub.Instance.Events;
        }


        private void Start()
        {
            Observable.EveryUpdate()
                .Select(_ => GetDirection())
                .Subscribe(d => eventsService.Input.ChangeDirectionMovement(d))
                .AddTo(gameObject);


            Observable.EveryUpdate()
                .Subscribe(_ =>
                {
                    Ray ray = GameManager.Instance.GameCamera.ScreenPointToRay(UnityEngine.Input.mousePosition);
                    Plane ground = new Plane(Vector3.down, Vector3.zero);
                    if (!ground.Raycast(ray, out float rayDistance))
                    {
                        return;
                    }

                    Vector3 point = ray.GetPoint(rayDistance);
                    #if UNITY_EDITOR
                        Debug.DrawLine(ray.origin, point, Color.red);
                    #endif

                    eventsService.Input.ChangeRotation(Quaternion.LookRotation(point));
                })
                .AddTo(gameObject);


            Observable.EveryUpdate()
                .Subscribe(_ =>
                {
                    if (TryGetSelectSlot(out int slotNumber))
                    {
                        eventsService.Input.ChangeWeaponSlot(slotNumber);
                    }
                })
                .AddTo(gameObject);


            //TODO: Need upgrade
            Observable.EveryUpdate()
                .Where(_ => UnityEngine.Input.GetMouseButtonDown(0))
                .Subscribe(_ => eventsService.Input.Shoot())
                .AddTo(gameObject);
        }

        #endregion



        #region Private methods

        private Vector3 GetDirection()
        {
            Vector3 direction = Vector3.zero;

            if (UnityEngine.Input.GetKey(KeyCode.W) || UnityEngine.Input.GetKey(KeyCode.UpArrow))
            {
                direction.z = 1.0f;
            }
            else if (UnityEngine.Input.GetKey(KeyCode.S) || UnityEngine.Input.GetKey(KeyCode.DownArrow))
            {
                direction.z = -1.0f;
            }

            if (UnityEngine.Input.GetKey(KeyCode.D) || UnityEngine.Input.GetKey(KeyCode.RightArrow))
            {
                direction.x = 1.0f;
            }
            else if (UnityEngine.Input.GetKey(KeyCode.A) || UnityEngine.Input.GetKey(KeyCode.LeftArrow))
            {
                direction.x = -1.0f;
            }

            return direction;
        }


        private bool TryGetSelectSlot(out int slotNumber)
        {
            bool result = true;
            if (UnityEngine.Input.GetKeyDown(KeyCode.Keypad1))
            {
                slotNumber = 1;
            }
            else if (UnityEngine.Input.GetKeyDown(KeyCode.Keypad2))
            {
                slotNumber = 2;
            }
            else if (UnityEngine.Input.GetKeyDown(KeyCode.Keypad3))
            {
                slotNumber = 3;
            }
            else
            {
                result = false;
                slotNumber = 0;
            }

            return result;
        }

        #endregion
    }
}
