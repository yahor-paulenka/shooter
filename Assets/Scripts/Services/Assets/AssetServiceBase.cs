using System;
using UnityEngine;
using Object = UnityEngine.Object;


namespace Shooter.Services.Assets
{
    public class AssetServiceBase
    {
        #region Protected methods

        protected T Instantiate<T>(string path, Transform parent) where T : Component
        {
            GameObject prefab = LoadPrefab(path);
            return Instantiate<T>(prefab, parent);
        }


        protected GameObject Instantiate(string path, Transform parent)
        {
            GameObject prefab = LoadPrefab(path);
            return Instantiate(prefab, parent);
        }

        #endregion



        #region Private methods

        private GameObject Instantiate(GameObject prefab, Transform parent) => Object.Instantiate(prefab, parent);


        private T Instantiate<T>(GameObject prefab, Transform parent) where T : Component
        {
            GameObject copy = Instantiate(prefab, parent);
            T component = copy.GetComponent<T>();
            if (component == null)
            {
                throw new InvalidOperationException();
            }

            return component;
        }


        private GameObject LoadPrefab(string path) => Resources.Load<GameObject>(path);

        #endregion
    }
}
