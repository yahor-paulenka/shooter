using Shooter.Level;
using UnityEngine;


namespace Shooter.Services.Assets
{
    public class LevelAssetsService : AssetServiceBase
    {
        public LevelController InstantiateLevelController(Transform parent, int levelNumber) =>
            Instantiate<LevelController>(Paths.Levels.GetPath(levelNumber), parent);
    }
}
