namespace Shooter.Services.Assets
{
    public class AssetsServicesImpl : IAssetsServices
    {
        public LevelAssetsService Level { get; } = new LevelAssetsService();
        public HeroAssetService Hero { get; } = new HeroAssetService();
        public WeaponAssetsServices Weapon { get; } = new WeaponAssetsServices();
    }
}
