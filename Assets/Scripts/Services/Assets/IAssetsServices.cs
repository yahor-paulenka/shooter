namespace Shooter.Services.Assets
{
    public interface IAssetsServices
    {
        LevelAssetsService Level { get; }
        HeroAssetService Hero { get; }
        WeaponAssetsServices Weapon { get; }
    }
}
