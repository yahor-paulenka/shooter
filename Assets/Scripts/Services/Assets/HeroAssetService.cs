using Shooter.Characters;
using Shooter.Characters.Skin;
using Shooter.Data;
using Shooter.Data.Heroes;
using UniRx;
using UnityEngine;


namespace Shooter.Services.Assets
{
    public class HeroAssetService : AssetServiceBase
    {
        public HeroCharacter InstantiateCurrentHero(Transform parent, Vector3 position, string type)
        {
            Skin skin = Instantiate<Skin>(Paths.Heroes.GetPathSkin(type), parent);
            skin.transform.position = position;

            HeroInfo info = DataHub.Instance.Heroes.GetInfo(type);
            HeroCharacter hero = skin.gameObject.AddComponent<HeroCharacter>();
            hero.Initialize(info, skin);

            return hero;
        }
    }
}
