using System;
using Shooter.Data;
using Shooter.Data.Weapons;
using Shooter.Weapon;
using Shooter.Weapon.Bullet;
using UnityEngine;


namespace Shooter.Services.Assets
{
    public class WeaponAssetsServices : AssetServiceBase
    {
        public IWeapon InstantiateWeapon(Transform parent, WeaponType type)
        {
            WeaponSkin skin = Instantiate<WeaponSkin>(Paths.Weapons.GetSkinPath(type), parent);
            WeaponInfo info = DataHub.Instance.Weapons.GetWeaponInfo(type);
            IWeapon weapon = type switch
            {
                WeaponType.Pistol => new PistolWeapon(skin, info),
                WeaponType.DoubleHoleRifle => new DoubleHoleRifleWeapon(skin, info),
                WeaponType.AssaultRifle => new AssaultRifleWeapon(skin, info),
                _ => throw new ArgumentOutOfRangeException()
            };

            return weapon;
        }


        public BulletBase InstantiateBullet(Transform parent, BulletType bulletType)
        {
            BulletBase bulletBase = Instantiate<BulletBase>(Paths.Weapons.GetBulletPath(bulletType), parent);

            //TODO: Here you can add a bullet component for buff or debuff
            switch (bulletType)
            {
                case BulletType.ElectricSphere:
                    break;
                case BulletType.SimpleSphere:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(bulletType), bulletType, null);
            }

            return bulletBase;
        }
    }
}
