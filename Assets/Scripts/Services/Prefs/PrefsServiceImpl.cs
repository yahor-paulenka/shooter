using Newtonsoft.Json;
using UnityEngine;


namespace Shooter.Services.Prefs
{
    public class PrefsServiceImpl : IPrefsService

    {
        #region Private methods

        private static void Save() => PlayerPrefs.Save();

        #endregion



        #region IPrefsService

        public bool HasKey(string key) => PlayerPrefs.HasKey(key);
        public void DeleteKey(string key) => PlayerPrefs.DeleteKey(key);


        public void SetString(string key, string value, bool isSaveImmediately = false)
        {
            PlayerPrefs.SetString(key, value);

            if (isSaveImmediately)
            {
                Save();
            }
        }


        public string GetString(string key) => GetString(key, string.Empty);
        public string GetString(string key, string defaultValue) => PlayerPrefs.GetString(key, defaultValue);


        public void SetObjectValue<T>(string key, T value, bool saveImmediately = false) where T : class
        {
            string objectValue = value == null ?
                string.Empty :
                JsonConvert.SerializeObject(value);
            SetString(key, objectValue, saveImmediately);
        }


        public T GetObjectValue<T>(string key, T defaultValue = null) where T : class
        {
            T result = defaultValue;
            string savedObjectValue = GetString(key);
            if (!string.IsNullOrEmpty(savedObjectValue))
            {
                result = JsonConvert.DeserializeObject<T>(savedObjectValue);
            }

            return result;
        }

        #endregion
    }
}
