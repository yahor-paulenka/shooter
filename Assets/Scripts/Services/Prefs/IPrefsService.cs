namespace Shooter.Services.Prefs
{
    public interface IPrefsService
    {
        bool HasKey(string key);
        void DeleteKey(string key);
        void SetString(string key, string value, bool isSaveImmediately = false);
        string GetString(string key);
        string GetString(string key, string defaultValue);
        void SetObjectValue<T>(string key, T value, bool saveImmediately = false) where T : class;
         T GetObjectValue<T>(string key, T defaultValue = null) where T : class;
    }
}
