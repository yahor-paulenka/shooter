using Shooter.Services.Assets;
using Shooter.Services.Events;
using Shooter.Services.Level;
using Shooter.Services.Prefs;


namespace Shooter.Services
{
    public class ServicesHub
    {
        #region Fields

        private static ServicesHub instance;

        #endregion



        #region Properties

        public static ServicesHub Instance => instance ??= new ServicesHub();


        public IAssetsServices Assets { get; }
        public IEventsService Events { get; }
        public IPrefsService Prefs { get; }
        public ILevelService Level { get; }

        #endregion



        #region Class lifecycle

        private ServicesHub()
        {
            Assets = new AssetsServicesImpl();
            Events = new EventsServiceImpl();
            Prefs = new PrefsServiceImpl();
            Level = new LevelServiceImpl(Assets);
        }

        #endregion
    }
}
