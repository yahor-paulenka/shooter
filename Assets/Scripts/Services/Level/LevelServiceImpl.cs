using Shooter.Game;
using Shooter.Level;
using Shooter.Services.Assets;
using Shooter.Services.Prefs;
using UnityEngine;


namespace Shooter.Services.Level
{
    public class LevelServiceImpl : ILevelService
    {
        #region Fields

        private readonly IAssetsServices assetsServices;

        #endregion



        #region Class lifecycle

        public LevelServiceImpl(IAssetsServices assetsServices)
        {
            this.assetsServices = assetsServices;
        }

        #endregion



        #region ILevelService

        public LevelController LevelController { get; private set; }


        public void CreateLevel()
        {
            DisposeActiveLevel();

            LevelController = assetsServices.Level.InstantiateLevelController(GameManager.Instance.GameRoot, 1);
        }


        public void DisposeActiveLevel()
        {
            if (LevelController == null)
            {
                return;
            }

            Object.Destroy(LevelController.gameObject);
            LevelController = null;
        }

        #endregion
    }
}
