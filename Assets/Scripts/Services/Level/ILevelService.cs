using Shooter.Level;


namespace Shooter.Services.Level
{
    public interface ILevelService
    {
        LevelController LevelController { get; }
        void CreateLevel();
        void DisposeActiveLevel();
    }
}
