namespace Shooter.Services.Events
{
    public class EventsServiceImpl : IEventsService
    {
        public LevelEvents Level { get; } = new LevelEvents();
        public InputEvents Input { get; } = new InputEvents();
    }
}
