using System;
using UniRx;
using UnityEngine;


namespace Shooter.Services.Events
{
    public class InputEvents
    {
        private readonly Subject<Vector3> directionMovementChanged = new Subject<Vector3>();
        private readonly Subject<Quaternion> rotationChanged = new Subject<Quaternion>();
        private readonly Subject<int> weaponSlotChanged = new Subject<int>();
        private readonly Subject<Unit> shot = new Subject<Unit>();


        public IObservable<Vector3> DirectionMovementChanged => directionMovementChanged.AsObservable();
        public IObservable<Quaternion> RotationChanged => rotationChanged.AsObservable();
        public IObservable<int> WeaponSlotChanged => weaponSlotChanged.AsObservable();
        public IObservable<Unit> Shot => shot.AsObservable();


        public void ChangeDirectionMovement(Vector3 direction) => directionMovementChanged.OnNext(direction);
        public void ChangeRotation(Quaternion quaternion) => rotationChanged.OnNext(quaternion);
        public void ChangeWeaponSlot(int slotNumber) => weaponSlotChanged.OnNext(slotNumber);
        public void Shoot() => shot.OnNext(Unit.Default);
    }
}
