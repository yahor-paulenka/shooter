namespace Shooter.Services.Events
{
    public interface IEventsService
    {
        LevelEvents Level { get; }
        InputEvents Input { get; }
    }
}
