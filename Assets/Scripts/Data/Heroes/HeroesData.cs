using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;


namespace Shooter.Data.Heroes
{
    [CreateAssetMenu(fileName = "Data_Heroes", menuName = "Data/Heroes Data")]
    public class HeroesData : SerializedScriptableObject
    {
        #region Fields

        [SerializeField] private Dictionary<string, HeroInfo> heroInfoByType;
        [ValueDropdown(nameof(GetAllTypes))]
        [SerializeField] [Required] private string defaultHeroType;

        #endregion



        #region Properties

        public string DefaultHeroType => defaultHeroType;

        #endregion


        #region Public methods

        public HeroInfo GetInfo(string heroType) => heroInfoByType[heroType];
        public string[] GetAllTypes() => heroInfoByType.Keys.ToArray();

        #endregion
    }
}
