using System;
using Shooter.Weapon;
using UnityEngine;


namespace Shooter.Data.Heroes
{
    [Serializable]
    public class HeroInfo
    {
        [SerializeField] private string name;
        [SerializeField] [Min(0.0f)] private float heals;
        [SerializeField] [Min(0.0f)] private float movementSpeed;
        [SerializeField] [Min(0.0f)] private float rotationSpeed;
        [SerializeField] private WeaponType[] weapons;


        public string Name => name;
        public float Heals => heals;
        public float MovementSpeed => movementSpeed;
        public float RotationSpeed => rotationSpeed;
        public WeaponType[] Weapons => weapons;
    }
}
