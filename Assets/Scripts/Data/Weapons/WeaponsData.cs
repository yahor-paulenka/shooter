using System;
using System.Linq;
using Shooter.Weapon;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Shooter.Data.Weapons
{
    [CreateAssetMenu(fileName = "Data_Weapons", menuName = "Data/Weapons Data")]
    public class WeaponsData : ScriptableObject
    {
        #region Fields

        [SerializeField] private WeaponInfo[] weapons;

        #endregion



        #region Public methods

        public WeaponInfo GetWeaponInfo(WeaponType weaponType)
        {
            if (weapons.All(i => i.WeaponType != weaponType))
            {
                throw new NullReferenceException();
            }

            return weapons.First(i => i.WeaponType == weaponType);
        }


        public WeaponInfo GetRandomWeapon() => weapons[Random.Range(0, weapons.Length)];

        #endregion
    }
}
