using System;
using Shooter.Weapon.Bullet;
using Shooter.Weapon;
using UnityEngine;


namespace Shooter.Data.Weapons
{
    [Serializable]
    public struct WeaponInfo
    {
        [SerializeField] private WeaponType weaponType;
        [SerializeField] private string name;
        [SerializeField] [Min(0.0f)] private float damage;
        [SerializeField] [Min(0.0f)] private float fireRate;
        [SerializeField] [Min(0.0f)] private float range;
        [SerializeField] [Range(0.0f, 180.0f)] private float spreadAngle;
        [SerializeField] [Min(0.0f)] private float timeReload;
        [SerializeField] [Min(1)] private int bulletCount;
        [SerializeField] private BulletType bulletType;


        public WeaponType WeaponType => weaponType;
        public string Name => name;
        public float Damage => damage;
        public float FireRate => fireRate;
        public float Range => range;
        public float SpreadAngle => spreadAngle;
        public float TimeReload => timeReload;
        public int BulletCount => bulletCount;
        public BulletType BulletType => bulletType;
    }
}
