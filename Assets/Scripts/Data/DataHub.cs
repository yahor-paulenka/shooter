using Shooter.Data.Heroes;
using Shooter.Data.Weapons;
using UnityEngine;


namespace Shooter.Data
{
    public class DataHub
    {
        #region Fields

        private static DataHub instance;


        private HeroesData heroesData;
        private WeaponsData weaponsData;

        #endregion



        #region Properties

        public static DataHub Instance => instance ??= new DataHub();

        public HeroesData Heroes => GetLazy(ref heroesData, "Data/Heroes/Data_Heroes");
        public WeaponsData Weapons => GetLazy(ref weaponsData, "Data/Weapons/Data_Weapons");

        #endregion




        #region Private methods

        private T GetLazy<T>(ref T backingStorage, string resourcePath) where T : ScriptableObject
        {
            if (backingStorage == null)
            {
                backingStorage = Resources.Load<T>(resourcePath);
            }

            return backingStorage;
        }


        #if UNITY_EDITOR

        [CustomSetup(Priority = -1000)]
        private static void CustomSetup()
        {
            instance = null;
        }

        #endif

        #endregion
    }
}
