namespace Shooter.Weapon.Bullet
{
    public enum BulletType
    {
        None = 0,
        SimpleSphere = 1,
        ElectricSphere = 2
    }
}
