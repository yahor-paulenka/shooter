using Shooter.Characters;
using Shooter.Characters.Skin;
using UniRx;
using UnityEngine;


namespace Shooter.Weapon.Bullet
{
    public class BulletBase : MonoBehaviour
    {
        [SerializeField] private Rigidbody body;

        private ISkin Sender { get; set; }
        private float Damage { get; set; }


        private void OnTriggerEnter(Collider other)
        {
            ICharacter character = other.gameObject.GetComponent<ICharacter>();
            if (character == null || character.Skin == Sender)
            {
                return;
            }

            character.StatsHandler.ChangeHealth(-Damage);
            Deactivation();
        }


        public void Fly(ISkin sender, float damage, float range, float speed = 5.0f)
        {
            Sender = sender;
            Damage = damage;

            Vector3 startPosition = body.position;
            Observable.EveryFixedUpdate()
                .Subscribe(_ =>
                {
                    if (Vector3.Distance(startPosition, body.position) >= range)
                    {
                        Deactivation();
                    }

                    Vector3 position = body.position;
                    position += body.transform.forward * (speed * Time.deltaTime);
                    body.MovePosition(position);
                })
                .AddTo(gameObject);
        }


        private void Deactivation()
        {
            Destroy(gameObject);
        }
    }
}
