using Shooter.Characters.Skin;
using Shooter.Data.Weapons;


namespace Shooter.Weapon
{
    public interface IWeapon
    {
        WeaponInfo Info { get; }

        void SetActive(bool value);
        void Shoot(ISkin sender);
    }
}
