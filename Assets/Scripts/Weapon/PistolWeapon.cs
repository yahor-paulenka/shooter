using Shooter.Characters.Skin;
using Shooter.Data.Weapons;
using Shooter.Services;
using Shooter.Services.Assets;
using Shooter.Weapon.Bullet;
using UniRx;


namespace Shooter.Weapon
{
    public class PistolWeapon : IWeapon
    {
        #region Fiels

        private readonly WeaponSkin skin;
        private readonly IAssetsServices assetsServices;

        #endregion



        #region Class lifecycle

        public PistolWeapon(WeaponSkin skin, WeaponInfo info)
        {
            this.skin = skin;
            Info = info;

            assetsServices = ServicesHub.Instance.Assets;

           var a = Observable.EveryUpdate()
                .Where(_ => skin.enabled)
                .Subscribe(_ =>
                {

                }).AddTo(skin.gameObject);
        }

        #endregion



        #region IWeapon

        public WeaponInfo Info { get; }


        public void SetActive(bool value)
        {
            skin.enabled = value;
        }


        public void Shoot(ISkin sender)
        {
            //TODO: Need a bullet pool
            BulletBase bullet = assetsServices.Weapon.InstantiateBullet(skin.MuzzleRoot, Info.BulletType);
            bullet.Fly(sender, Info.Damage, Info.Range);
        }

        #endregion
    }
}
