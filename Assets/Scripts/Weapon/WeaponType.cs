namespace Shooter.Weapon
{
    public enum WeaponType
    {
        None = 0,
        Pistol = 1,
        DoubleHoleRifle = 2,
        AssaultRifle = 3
    }
}
