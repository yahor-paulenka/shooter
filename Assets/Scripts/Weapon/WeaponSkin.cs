using UnityEngine;


namespace Shooter.Weapon
{
    public class WeaponSkin : MonoBehaviour
    {
        [SerializeField] Transform muzzleRoot;

        public Transform MuzzleRoot => muzzleRoot;
    }
}
