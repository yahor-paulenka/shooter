using Shooter.Characters.Skin;
using Shooter.Data.Weapons;


namespace Shooter.Weapon
{
    public class AssaultRifleWeapon : IWeapon
    {
        #region Fiels

        private WeaponSkin skin;

        #endregion



        #region Class lifecycle

        public AssaultRifleWeapon(WeaponSkin skin, WeaponInfo info)
        {
            this.skin = skin;
            Info = info;
        }

        #endregion



        #region IWeapon

        public WeaponInfo Info { get; }


        public void SetActive(bool value)
        {
            skin.enabled = value;
        }


        public void Shoot(ISkin sender)
        {
        }

        #endregion
    }
}
