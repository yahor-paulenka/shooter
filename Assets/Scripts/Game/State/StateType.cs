namespace Shooter.Game
{
    public enum StateType
    {
        None = 0,
        InGame = 1,
        Result = 2,
        Restart = 3
    }
}
