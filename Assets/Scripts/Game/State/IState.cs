namespace Shooter.Game
{
    public interface IState
    {
        StateType Type { get; }
        void OnEnter();
        void OnExit();
    }
}
