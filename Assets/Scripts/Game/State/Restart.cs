namespace Shooter.Game
{
    public class Restart : IState
    {
        #region IState

        public StateType Type => StateType.Restart;
        public void OnEnter() => GameManager.Instance.SetGameState(StateType.InGame);
        public void OnExit() { }

        #endregion
    }
}
