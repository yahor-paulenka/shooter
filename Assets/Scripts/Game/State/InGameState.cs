using Shooter.Services;


namespace Shooter.Game
{
    public class InGameState : IState
    {
        #region IState

        public StateType Type => StateType.InGame;


        public void OnEnter()
        {
            ServicesHub.Instance.Level.CreateLevel();
        }


        public void OnExit()
        {
            ServicesHub.Instance.Level.DisposeActiveLevel();
        }

        #endregion
    }
}
