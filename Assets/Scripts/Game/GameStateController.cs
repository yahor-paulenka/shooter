using System.Linq;


namespace Shooter.Game
{
    public class GameStateController
    {
        private readonly IState[] states;
        private IState previousState;


        public GameStateController()
        {
            states = new IState[]
            {
                new InGameState(),
                new Result(),
                new Restart()
            };
        }


        public void SetState(StateType stateType)
        {
            if (previousState != null)
            {
                previousState.OnExit();
            }

            previousState = states.First(s => s.Type == stateType);
            previousState.OnEnter();
        }
    }
}
