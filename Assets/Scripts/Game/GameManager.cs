using UnityEngine;


namespace Shooter.Game
{
    public class GameManager : SingletonMonoBehaviour<GameManager>
    {
        #region Fields

        [SerializeField] private Transform gameRoot;
        [SerializeField] private Camera gameCamera;

        private GameStateController stateController;

        #endregion



        #region Properties

        public Transform GameRoot => gameRoot;
        public Camera GameCamera => gameCamera;

        #endregion



        #region Unity lifecycle

        private void Awake()
        {
            DontDestroyOnLoad(this);

            Application.targetFrameRate = 60;
            UnityEngine.Input.multiTouchEnabled = false;

            stateController = new GameStateController();
        }


        private void Start()
        {
            SetGameState(StateType.InGame);
        }

        #endregion



        #region Public methods

        public void SetGameState(StateType stateType) => stateController.SetState(stateType);

        #endregion
    }
}
