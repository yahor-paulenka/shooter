using Shooter.Characters;
using Shooter.Data;
using Shooter.Game;
using Shooter.Input;
using Shooter.Services;
using Shooter.Services.Assets;
using UniRx;
using UnityEngine;


namespace Shooter.Level
{
    public class LevelController : MonoBehaviour
    {
        #region Fields

        [SerializeField] private Transform heroSpawnRoot;

        private Vector3 cameraVelocity;

        #endregion




        #region Unity lifecycle

        private void Awake()
        {
            IAssetsServices assetsServices = ServicesHub.Instance.Assets;
            HeroCharacter hero = assetsServices.Hero.InstantiateCurrentHero(transform,
                heroSpawnRoot.position, DataHub.Instance.Heroes.DefaultHeroType);

            gameObject.AddComponent<InputHandler>();

            SetCameraFollow(hero);
        }

        #endregion



        #region Private methods

        private void SetCameraFollow(HeroCharacter hero)
        {
            Observable.EveryLateUpdate()
                .Select(_ => hero.Skin.Rigidbody.position)
                .Subscribe(p =>
                {
                    Camera gameCamera = GameManager.Instance.GameCamera;
                    if (gameCamera == null)
                        return;

                    Vector3 targetPosition = p;
                    targetPosition.y = gameCamera.transform.position.y;
                    targetPosition.z -= 10.0f;

                    gameCamera.transform.position = Vector3.SmoothDamp(gameCamera.transform.position,
                        targetPosition, ref cameraVelocity, 0.3f);
                })
                .AddTo(hero.gameObject);
        }

        #endregion
    }
}
