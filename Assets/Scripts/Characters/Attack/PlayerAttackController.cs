using Shooter.Characters.Skin;
using Shooter.Services;
using Shooter.Services.Assets;
using Shooter.Services.Events;
using Shooter.Weapon;
using UniRx;
using UnityEngine;


namespace Shooter.Characters.Attack
{
    public class PlayerAttackController : IAttackController
    {
        #region Fields

        private const int InventorySize = 3;
        private IWeapon[] weapons;
        private IWeapon selectedWeapon;

        #endregion



        #region Class lifecycle

        public PlayerAttackController(ISkin skin, WeaponType[] weaponTypes)
        {
            ServicesHub servicesHub = ServicesHub.Instance;
            IAssetsServices assetsServices = servicesHub.Assets;
            IEventsService eventsService = servicesHub.Events;

            weapons = new IWeapon[InventorySize];
            for (int i = 0; i < InventorySize; i++)
            {
                WeaponType type = i < weaponTypes.Length ?
                    weaponTypes[i] :
                    WeaponType.None;

                weapons[i] = type == WeaponType.None ?
                    null :
                    assetsServices.Weapon.InstantiateWeapon(skin.HandRoot, type);
            }

            ChooseWeapon(1);

            eventsService.Input.WeaponSlotChanged
                .Subscribe(ChooseWeapon)
                .AddTo(skin.Root);

            eventsService.Input.Shot
                .Where(_ => selectedWeapon != null)
                .Subscribe(_ => selectedWeapon.Shoot(skin))
                .AddTo(skin.Root);
        }

        #endregion



        #region Private methods

        private void ChooseWeapon(int slotNumber)
        {
            int index = Mathf.Clamp(slotNumber - 1, 0, InventorySize);
            for (int i = 0; i < InventorySize; i++)
            {
                if (i == index)
                {
                    selectedWeapon = weapons[i];
                    selectedWeapon.SetActive(true);
                }
                else
                {
                    weapons[i].SetActive(false);
                }
            }
        }

        #endregion
    }
}
