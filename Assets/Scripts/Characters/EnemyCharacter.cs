using Shooter.Characters.Attack;
using Shooter.Characters.Movement;
using Shooter.Characters.Skin;
using Shooter.Characters.Stats;
using Shooter.Data;
using UnityEngine;


namespace Shooter.Characters
{
    public class EnemyCharacter : MonoBehaviour, ICharacter
    {
        #region Public methods

        public void Initialize(ISkin skin)
        {
            //TODO: Enemies need Info too
            Skin = skin;
            StatsHandler = new StatsHandler(500, 10, 10);
            MovementController = new NavMeshMovementController(Skin);
            AttackController = new AutoAimAttackController(Skin, DataHub.Instance.Weapons.GetRandomWeapon());
        }

        #endregion



        #region ICharacter

        public ISkin Skin { get; private set; }
        public IStatsHandler StatsHandler { get; private set; }
        public IMovementController MovementController { get; private set; }
        public IAttackController AttackController { get; private set; }

        #endregion
    }
}
