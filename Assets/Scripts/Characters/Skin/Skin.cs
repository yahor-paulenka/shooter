using UnityEngine;


namespace Shooter.Characters.Skin
{
    public class Skin : MonoBehaviour, ISkin
    {
        #region Fields

        [SerializeField] private Transform handRoot;
        [SerializeField] private Rigidbody rigidbody;

        #endregion



        #region ISkin

        public GameObject Root => gameObject;
        public Transform HandRoot => handRoot;
        public Rigidbody Rigidbody => rigidbody;

        #endregion
    }
}
