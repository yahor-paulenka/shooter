using UnityEngine;


namespace Shooter.Characters.Skin
{
    public interface ISkin
    {
        GameObject Root { get; }
        Transform HandRoot { get; }
        Rigidbody Rigidbody { get; }
    }
}
