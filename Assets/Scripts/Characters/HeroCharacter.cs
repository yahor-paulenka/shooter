using Shooter.Characters.Attack;
using Shooter.Characters.Movement;
using Shooter.Characters.Skin;
using Shooter.Characters.Stats;
using Shooter.Data.Heroes;
using UnityEngine;


namespace Shooter.Characters
{
    public class HeroCharacter : MonoBehaviour, ICharacter
    {
        #region Public methods

        public void Initialize(HeroInfo info, ISkin skin)
        {
            Skin = skin;
            StatsHandler = new StatsHandler(info.Heals, info.MovementSpeed, info.RotationSpeed);
            MovementController = new PayerMovementController(Skin, StatsHandler);
            AttackController = new PlayerAttackController(Skin, info.Weapons);
        }

        #endregion



        #region ICharacter

        public ISkin Skin { get; private set; }
        public IStatsHandler StatsHandler { get; private set; }
        public IMovementController MovementController { get; private set; }
        public IAttackController AttackController { get; private set; }

        #endregion
    }
}
