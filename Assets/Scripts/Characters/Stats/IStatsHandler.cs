namespace Shooter.Characters.Stats
{
    public interface IStatsHandler
    {
        float MaxHeals { get; }
        float Health { get; }
        bool IsDead { get; }
        public float MovementSpeed { get; }
        public float RotationSpeed { get; }



        void ChangeHealth(float value);
    }
}
