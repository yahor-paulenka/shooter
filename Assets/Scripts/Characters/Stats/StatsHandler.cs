using UnityEngine;


namespace Shooter.Characters.Stats
{
    public class StatsHandler : IStatsHandler
    {
        #region Class lifecycle

        public StatsHandler(float maxHeals, float movementSpeed, float rotationSpeed)
        {
            MaxHeals = maxHeals;
            Health = maxHeals;
            MovementSpeed = movementSpeed;
            RotationSpeed = rotationSpeed;
        }

        #endregion



        #region IStatsHandler

        public float MaxHeals { get; private set; }
        public float Health { get; private set; }
        public bool IsDead => Health <= 0.0f;
        public float MovementSpeed { get; private set;}
        public float RotationSpeed { get; private set;}


        public void ChangeHealth(float value)
        {
            Health = Mathf.Clamp(Health + value, 0.0f, MaxHeals);
        }

        #endregion
    }
}
