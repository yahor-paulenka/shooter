using Shooter.Characters.Skin;
using Shooter.Characters.Stats;
using Shooter.Services;
using Shooter.Services.Events;
using UniRx;
using UnityEngine;


namespace Shooter.Characters.Movement
{
    public class PayerMovementController : IMovementController
    {
        #region Class lifecycle

        public PayerMovementController(ISkin skin, IStatsHandler statsHandler)
        {
            ServicesHub servicesHub = ServicesHub.Instance;
            IEventsService eventsService = servicesHub.Events;

            eventsService.Input.DirectionMovementChanged
                .Where(_ => !statsHandler.IsDead)
                .Subscribe(direction =>
                {
                    Vector3 position = skin.Rigidbody.position;
                    position += direction * (statsHandler.MovementSpeed * Time.deltaTime);
                    skin.Rigidbody.MovePosition(position);
                })
                .AddTo(skin.Root);

            eventsService.Input.RotationChanged
                .Where(_ => !statsHandler.IsDead)
                .Subscribe(rotation =>
                {
                    Quaternion target = Quaternion.Lerp(skin.Rigidbody.rotation,
                        rotation, Time.deltaTime * statsHandler.RotationSpeed);
                    skin.Rigidbody.MoveRotation(target);
                })
                .AddTo(skin.Root);
        }

        #endregion
    }
}
