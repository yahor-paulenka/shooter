using Shooter.Characters.Attack;
using Shooter.Characters.Movement;
using Shooter.Characters.Skin;
using Shooter.Characters.Stats;


namespace Shooter.Characters
{
    public interface ICharacter
    {
        ISkin Skin { get; }
        IStatsHandler StatsHandler { get; }
        IMovementController MovementController { get; }
        IAttackController AttackController { get; }
    }
}
