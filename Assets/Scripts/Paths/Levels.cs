namespace Paths
{
    public static class Levels
    {
        private const string Root = Common.PrefabsRoot + "Levels/";
        private const string PrefixLevel = "Levels_";

        public static string GetPath(int number) => Root + PrefixLevel + number;
    }
}
