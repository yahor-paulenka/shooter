using Shooter.Weapon;
using Shooter.Weapon.Bullet;


namespace Paths
{
    public static class Weapons
    {
        private const string Root = Common.PrefabsRoot + "Weapons/";
        private const string PrefixSkin = "Weapons_";
        private const string BulletsRoot = Root + "Bullets/";
        private const string PrefixBullet = "Weapons_Bullets_";

        public static string GetSkinPath(WeaponType weaponType) => Root + PrefixSkin + weaponType;
        public static string GetBulletPath(BulletType bulletType) => BulletsRoot + PrefixBullet + bulletType;
    }
}
