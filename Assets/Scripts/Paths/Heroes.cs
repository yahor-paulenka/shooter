using Shooter.Data.Heroes;


namespace Paths
{
    public static class Heroes
    {
        private const string SkinsRoot = Common.PrefabsRoot + "Heroes/Skins/";
        private const string PrefixSkin = "Heroes_Skins_";

        public static string GetPathSkin(string heroType) => SkinsRoot + PrefixSkin + heroType;
    }
}
